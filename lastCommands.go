package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

type LastCommands map[int64]string

func NewLC() *LastCommands {
	return &LastCommands{}
}

func (lc *LastCommands) Save() error {
	b, _ := json.Marshal(lc)
	err := ioutil.WriteFile("lastCommands.json", b, 777)
	return err
}

func (lc *LastCommands) Load() {
	file, err := os.Open("lastCommands.json")
	if err != nil {
		log.Panic(err)
	}

	defer file.Close()

	dec := json.NewDecoder(file)
	dec.Decode(&lc)
}

func (lc *LastCommands) Set(id int64, com string) {
	(*lc)[id] = com
	lc.Save()
}

func (lc *LastCommands) Get(id int64) (string, bool) {
	com, ok := (*lc)[id]
	return com, ok
}

func (lc *LastCommands) Del(id int64) {
	delete(*lc, id)
	lc.Save()
}
