package main

import (
	"bufio"
	"fmt"
	"os"
)

// var (
// 	u *User
// )

func Test() {
	// ctx, cancel := context.WithCancel(context.Background())
	// defer cancel()
	// go scanner(ctx)
	u := NewUser(0)
	u.SetBalance(150000)
	u.SetDayLimit(30)
	fmt.Println(u.Available())
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		switch scanner.Text() {
		case "/exit":
			for _, v := range u.GetAllSpendingsByAllCategories() {
				fmt.Printf("%#v\n", v)
			}
			return
		default:
			fmt.Println(u.AddSpending(scanner.Text()))
		}
	}
}

//scanner читает из консоли
// func scanner(ctx context.Context) {
// 	scanner := bufio.NewScanner(os.Stdin)
// 	select {
// 	case <-ctx.Done():
// 		return
// 	default:
// 		if scanner.Scan() {
// 			u.AddSpending(scanner.Text())
// 		}
// 	}
// }
