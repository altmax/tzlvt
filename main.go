package main

import (
	"html"
	"log"
	"time"

	"github.com/go-telegram-bot-api/telegram-bot-api"
)

var (
	chats   = Chats{Tasks: map[int64]*Task{}, Users: map[int64]*User{}}
	buttons = [][]tgbotapi.KeyboardButton{
		[]tgbotapi.KeyboardButton{
			tgbotapi.KeyboardButton{Text: "/balance"},
			tgbotapi.KeyboardButton{Text: "/leftDay"},
		},
	}
	keyboard = tgbotapi.ReplyKeyboardMarkup{
		ResizeKeyboard: true,
		Keyboard:       buttons,
	}
	lastCommands = NewLC()
)

func main() {
	Test()
	return
	chats.Load()

	go chats.ParallelSave()
	go chats.TaskSend()
	go Reprocess()
	bot, err := tgbotapi.NewBotAPI("397842105:AAHpHwET7vCjUw6aoE5c3tImFgxxQr0vVa0")
	if err != nil {
		log.Panic(err)
	}
	chats.Bot = bot
	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)

	u.Timeout = 60
	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		chatID := update.Message.Chat.ID

		answer := ""

		if update.Message.IsCommand() {
			switch update.Message.Command() {
			case "start":
				answer = Start(update)
			case "stop":
				answer = Stop(update)
			case "help":
				answer = GetHelpInfo()
			case "leftDay":
				answer = SetLastCommand(update, "leftday")
			case "balance":
				answer = CurBalance(update)
			case "testnewday":
				NewDaysForAll()
				// answer = CurBalance(update)
			}
		} else {
			lc, ok := lastCommands.Get(chatID)
			if ok {
				switch lc {
				case "start":
					answer = StartBalance(update)
				case "leftday":
					answer = AddLeftDay(update)
				}
			} else {
				answer = AddSpending(update)
			}
		}

		msg := tgbotapi.NewMessage(chatID, html.UnescapeString(answer))
		msg.ReplyMarkup = keyboard
		bot.Send(msg)

	}
}

func Reprocess() {
	for {
		select {
		case <-time.After(time.Duration(24-time.Now().Hour()) * time.Hour):
			NewDaysForAll()
		}
	}
}

func NewDaysForAll() {
	for _, v := range chats.Users {
		v.NewDay()
		chats.Send(v.ID, v.Available())
	}
}
