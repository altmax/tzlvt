package main

import (
	"fmt"
	"strconv"

	"github.com/go-telegram-bot-api/telegram-bot-api"
)

func Start(upd tgbotapi.Update) string {
	chatID := upd.Message.Chat.ID
	var err error
	if !chats.Find(chatID) {
		err = chats.Add(chatID)
	}

	if err != nil {
		return "Привет. Что-то пошло не так. Попробуй стартовать меня чуть позже:("
	}

	lastCommands.Set(chatID, "start")

	return "Привет! Я буду вести учет твоих расходов. Точнее, я просто буду говорить тебе, сколько ты можешь потратить сегодня и записывать твои траты. Для начала введи своё кол-во денег, которое ты имеешь на текущий момент."
}

func Stop(upd tgbotapi.Update) string {
	chatID := upd.Message.Chat.ID

	if chats.Find(chatID) {
		chats.Delete(chatID)
	}
	lastCommands.Del(chatID)
	return "Возвращайся!"
}

func GetHelpInfo() string {
	return `
	/getSpendings - Показать расходы
	`
}

func StartBalance(upd tgbotapi.Update) string {
	chatID := upd.Message.Chat.ID

	u := chats.GetUser(chatID)
	bs := upd.Message.Text
	b, err := strconv.ParseInt(bs, 10, 64)
	if err != nil {
		return err.Error()
	}
	u.Balance = b
	lastCommands.Set(chatID, "leftday")
	return `Супер. Теперь введи количество дней, которое ты будешь это тратить.`
}

func AddLeftDay(upd tgbotapi.Update) string {
	chatID := upd.Message.Chat.ID

	days := upd.Message.Text
	day, err := strconv.ParseInt(days, 10, 64)
	if err != nil {
		return `It's not a number! Try again.`
	}
	u := chats.Users[chatID]
	u.SetDayLimit(day)

	lastCommands.Del(chatID)

	return u.Available()
}

func AddSpending(upd tgbotapi.Update) string {
	chatID := upd.Message.Chat.ID

	u := chats.GetUser(chatID)
	spendS := upd.Message.Text
	if spendS == "" {
		return "Error"
	}
	return u.AddSpending(spendS)
}

func CurBalance(upd tgbotapi.Update) string {
	chatID := upd.Message.Chat.ID

	u := chats.GetUser(chatID)

	return fmt.Sprintf("balance  %d\n in curDay  %d", u.Balance, u.CurDayBalance)
}

func SetLastCommand(upd tgbotapi.Update, comm string) string {
	chatID := upd.Message.Chat.ID
	lastCommands.Set(chatID, comm)
	return "Вводи"
}
