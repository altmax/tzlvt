package main

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

type User struct {
	ID            int64 `json:"id"`
	Spendings     []Spending
	Balance       int64
	DaysLeft      int64
	CurDayBalance int64
	Timezone      int64
}

func NewUser(id int64) *User {
	u := &User{}
	u.ID = id
	u.Spendings = make([]Spending, 0)
	u.Balance = 0
	u.DaysLeft = 0
	u.CurDayBalance = 0
	u.Timezone = 3
	return u
}

func (u *User) SetBalance(sum int64) {
	u.Balance = sum
}

type Spending struct {
	Name    string
	Sum     int64
	Sign    bool
	Comment string
}

func (u *User) AddSpending(str string) string {
	spend := strings.SplitN(str, " ", 3)
	if len(spend) < 3 {
		if len(spend) < 2 {
			spend = append(spend, "default_category")
		}
		spend = append(spend, "default_comment")
	}
	if spend[0] == "" {
		return "Error: sum can not be empty"
	}
	koef := int64(1)
	if spend[0][:1] != "+" {
		koef = -1
	}
	sum, err := strconv.ParseInt(spend[0], 10, 64)
	if err != nil {
		return err.Error()
	}
	u.addSpending(spend[1], spend[2], sum*koef)
	return u.DayBalance()
}

func (u *User) addSpending(name, comment string, sum int64) {
	s := Spending{}
	s.Comment = comment
	s.Name = name
	s.Sum = sum
	u.Spendings = append(u.Spendings, s)
	u.Balance += sum
	u.CurDayBalance += sum

}

func (u *User) SetDayLimit(dl int64) {
	u.DaysLeft = dl
	u.process()
}

func (u *User) process() {
	u.CurDayBalance = int64(u.Balance / u.DaysLeft)
}

func (u *User) DayBalance() string {
	if u.CurDayBalance > 0 {
		return fmt.Sprintf("You may spend %d today", u.CurDayBalance)
	}
	return fmt.Sprintf("Ты превысил свой лимит на %.0f, ебаный шопоголик", math.Abs(float64(u.CurDayBalance)))
}

func (u *User) NewDay() {
	u.DaysLeft--
	u.process()
}

func (u *User) Available() string {
	return fmt.Sprintf("Ok. You can spend %d at %d day(s). You may spend %d per day", u.Balance, u.DaysLeft, u.CurDayBalance)
}

func (u *User) GetAllSpendingsByAllCategories() []Spending {
	retSpend := make([]Spending, 0)
	founded := make(map[string]bool)
	for _, v := range u.Spendings {
		if _, ok := founded[v.Name]; !ok {
			founded[v.Name] = true
			sp := Spending{}
			sp.Name = v.Name
			for _, s := range u.Spendings {
				if s.Name == v.Name {
					sp.Sum += int64(math.Abs(float64(s.Sum)))
				}
			}
			retSpend = append(retSpend, sp)
		}
	}
	return retSpend
}
